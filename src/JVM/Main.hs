module JVM.Main where

import JVM.Compiler
import Utils

main :: IO ()
main = getFilename >>= createJVM
