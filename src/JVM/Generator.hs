module JVM.Generator where

import Control.Monad.State
import Control.Monad.Reader
import Data.Map.Strict as Map
import System.FilePath.Posix

import Instant.AbsInstant
import Utils

type JState = ([String], Map Ident JVar, Integer)

type JCompiler a = State JState a

type JVar = String

-- JCompiler Tools

addLine :: String -> JCompiler ()
addLine line = modify (\(lines, map, x) -> (line:lines, map, x))

addLines :: [String] -> JCompiler ()
addLines lines = Prelude.foldl (>>) (return ()) (Prelude.map addLine lines)

getLocalsNum :: JCompiler Integer
getLocalsNum = get >>= \(lines, map, x) -> return x

incLocalsNum :: JCompiler ()
incLocalsNum = modify (\(lines, map, x) -> (lines, map, x + 1))

getLocalsVar :: JCompiler JVar
getLocalsVar = get >>= \(lines, map, x) -> incLocalsNum >> return (show x)

insertToMap :: Ident -> JVar -> JCompiler ()
insertToMap ident label = modify (\(lines, map, x) -> (lines, Map.insert ident label map, x))

lookupIdent :: Ident -> JCompiler (Maybe JVar)
lookupIdent ident = get >>= \(lines, map, x) -> return $ Map.lookup ident map

getIdent :: Ident -> JCompiler JVar
getIdent ident = get >>= \(lines, map, x) -> case Map.lookup ident map of
  (Just var)  -> return var
  Nothing     -> return "error"

-- State

initState :: String -> Program -> JState
initState filename prog = (reverse [ ".class public " ++ (takeBaseName filename)
                           , ".super java/lang/Object"
                           , "; standard initializer"
                           , ".method public <init>()V"
                           , "aload_0"
                           , "invokespecial java/lang/Object/<init>()V"
                           , "return"
                           , ".end method"
                           , ".method public static main([Ljava/lang/String;)V"
                           ], Map.empty, 1)

finishState :: JState -> JState
finishState (lines, map, x) = (".end method":"return":lines, map, x)

-- Count Functions

countProgramLocals :: Program -> JCompiler Integer
countProgramLocals (Prog stmts) = do
  Prelude.foldl (>>) (return ()) (Prelude.map countStmtLocals stmts)
  value <- getLocalsNum
  addLine (localsLine value)
  return value

countStmtLocals :: Stmt -> JCompiler ()
countStmtLocals (SAss ident _) = do
  loc <- lookupIdent ident
  case loc of
    (Just _) -> return ()
    Nothing  -> do
      var <- getLocalsVar
      insertToMap ident var
countStmtLocals _ = return ()

countProgramStack :: Program -> JCompiler Integer
countProgramStack (Prog stmts) = do
    height <- maxStack (Prelude.map countStmtStack stmts)
    addLine (stackLine height) >> return height
  where countStmtStack (SAss _ exp) = countExpStack exp
        countStmtStack (SExp exp)   = countExpStack exp >>= return . (max 2)
        maxStack []           = return 1
        maxStack (stack:rest) = do
          stackHeight <- stack
          restHeight <- maxStack rest
          return $ max stackHeight restHeight

countExpStack :: Exp -> JCompiler Integer
countExpStack (ExpLit _) = return 1
countExpStack (ExpVar _) = return 1
countExpStack exp = do
    s1 <- countExpStack exp1
    s2 <- countExpStack exp2
    return $ min (max s1 (s2 + 1)) (max (s1 + 1) s2)
  where (exp1, exp2) = getSubExpressions exp

-- Code Line Operations

localsLine :: Integer -> String
localsLine x = ".limit locals " ++ show x

stackLine :: Integer -> String
stackLine x = ".limit stack " ++ show x

pushLine :: Integer -> String
pushLine x = "ldc " ++ show x

emptyLine :: String
emptyLine = ""

errorLine :: String
errorLine = "invalid"

operationLine :: Exp -> String
operationLine (ExpAdd _ _)  = "iadd"
operationLine (ExpSub _ _)  = "isub"
operationLine (ExpMul _ _)  = "imul"
operationLine (ExpDiv _ _)  = "idiv"
operationLine _             = errorLine

printLinesShallow :: String -> [String]
printLinesShallow load = [ "getstatic java/lang/System/out Ljava/io/PrintStream;"
                         , load
                         , "invokevirtual java/io/PrintStream/println(I)V" ]

printLines :: [String]
printLines = [ "getstatic java/lang/System/out Ljava/io/PrintStream;"
             , swapLine
             , "invokevirtual java/io/PrintStream/println(I)V"]

storeLine :: JVar -> String
storeLine var = "istore " ++ var

loadLine :: JVar -> String
loadLine var = "iload " ++ var

swapLine :: String
swapLine = "swap"

-- Create Functions

createProgram :: Program -> JCompiler ()
createProgram (Prog stmts) = Prelude.foldl (>>) (addLine emptyLine) (Prelude.map createStmt stmts)

createStmt :: Stmt -> JCompiler ()
createStmt (SAss ident exp) = do
  createExpression exp
  var <- getIdent ident
  addLine (storeLine var)
  addLine emptyLine

createStmt (SExp (ExpVar ident)) = do
  var <- getIdent ident
  addLines (printLinesShallow (loadLine var))
  addLine emptyLine
createStmt (SExp (ExpLit x)) = do
  addLines (printLinesShallow (pushLine x))
  addLine emptyLine
createStmt (SExp exp) = do
  createExpression exp
  addLines (printLines)
  addLine emptyLine

createExpression :: Exp -> JCompiler ()
createExpression (ExpVar ident) = do
  var <- getIdent ident
  addLine (loadLine var)
createExpression (ExpLit x) = addLine (pushLine x)

createExpression exp = do
    s1 <- countExpStack exp1
    s2 <- countExpStack exp2
    if s1 >= s2
      then createExpression exp1 >> createExpression exp2 >> addLine (operationLine exp)
      else createExpression exp2 >> createExpression exp1 >> if isCommutative exp
        then addLine (operationLine exp)
        else addLine swapLine >> addLine (operationLine exp)
  where (exp1, exp2) = getSubExpressions exp


generateProgram :: String -> Program -> IO [String]
generateProgram filename program = return lines
  where localsState     = execState (countProgramLocals program) (initState filename program)
        stackState      = execState (countProgramStack program) localsState
        programmedState = execState (createProgram program) stackState
        (lines, _, _)   = finishState programmedState
