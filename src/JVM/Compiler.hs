module JVM.Compiler where

import Data.List
import System.FilePath.Posix

import JVM.Generator
import Utils

helloJVM :: String -> IO [String]
helloJVM filename = return $ reverse
  [ ".class public " ++ (takeBaseName filename)
  , ".super java/lang/Object"
  , "; standard initializer"
  , ".method public <init>()V"
  , "aload_0"
  , "invokespecial java/lang/Object/<init>()V"
  , "return"
  , ".end method"
  , ".method public static main([Ljava/lang/String;)V"
  , ".limit stack 2"
  , "getstatic java/lang/System/out Ljava/io/PrintStream;"
  , "ldc \"Hello\""
  , "invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V"
  , "return"
  , ".end method" ]

createJVM :: String -> IO ()
createJVM filename = parseProgram filename >>= generateProgram filename >>= saveProgram (replaceExtension filename "j")
  >> linkJVM filename
  >> putStrLn "JVM compilation complete."

linkJVM :: String -> IO ()
linkJVM filename = runCommandWithExit $ "java -jar lib/jasmin.jar -d " ++ (takeDirectory filename) ++ " " ++ (replaceExtension filename "j")
