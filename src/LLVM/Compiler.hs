module LLVM.Compiler where

import System.FilePath.Posix

import LLVM.Generator
import Utils

helloLLVM :: IO [String]
helloLLVM = return $ reverse
  [ "declare void @printInt(i32)"
  , "define i32 @main() {"
  , "%i1 = add i32 2200, 37"
  , "call void @printInt(i32 %i1)"
  , "ret i32 0"
  , "}" ]


compileLLVM :: String -> IO ()
compileLLVM filename = parseProgram filename >>= generateProgram >>= saveProgram (replaceExtension filename "ll")
  >> linkLLVM filename
  >> putStrLn "LLVM compilation complete."

linkLLVM :: String -> IO ()
linkLLVM filename = runCommandsWithExit
  [ "llvm-as -o " ++ (replaceFileName filename "runtime.bc") ++ " lib/runtime.ll"
  , "llvm-as -o " ++ (replaceExtension filename "bcx") ++ " " ++(replaceExtension filename "ll")
  , "llvm-link -o " ++ (replaceExtension filename "bc") ++ " " ++ (replaceExtension filename "bcx") ++ " " ++ (replaceFileName filename "runtime.bc")
  , "rm -rf " ++ (replaceFileName filename "runtime.bc") ++ " " ++ (replaceExtension filename "bcx")]
