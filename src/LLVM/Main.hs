module LLVM.Main where

import LLVM.Compiler
import Utils

main :: IO ()
main = getFilename >>= compileLLVM
