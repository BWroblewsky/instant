module LLVM.Generator where

import Control.Monad.State
import Control.Monad.Reader
import Data.Map.Strict as Map

import Instant.AbsInstant
import Utils

type LState = ([String], Map Ident LLabel, Integer)

type LCompiler a = State LState a

type LLabel = String
data LValue = Label LLabel | Value Integer

instance Show LValue where
  show (Label l) = l
  show (Value x) = show x

-- LCompiler Tools

addLine :: String -> LCompiler ()
addLine line = modify (\(lines, map, x) -> (line:lines, map, x))

incLabelNum :: LCompiler ()
incLabelNum = modify (\(lines, map, x) -> (lines, map, x + 1))

getLabelVar :: LCompiler LLabel
getLabelVar = incLabelNum >> get >>= \(lines, map, x) -> incLabelNum >> return ("%v" ++ show x)

getLabelLine :: LCompiler LLabel
getLabelLine = incLabelNum >> get >>= \(lines, map, x) -> incLabelNum >> return ("%l" ++ show x)

insertToMap :: Ident -> LLabel -> LCompiler ()
insertToMap ident label = modify (\(lines, map, x) -> (lines, Map.insert ident label map, x))

lookupIdent :: Ident -> LCompiler (Maybe LLabel)
lookupIdent ident = get >>= \(lines, map, x) -> return $ Map.lookup ident map

getIdent :: Ident -> LCompiler LLabel
getIdent ident = get >>= \(lines, map, x) -> case Map.lookup ident map of
  (Just label)  -> return label
  Nothing       -> return "error"

-- State

initState :: LState
initState = (["define i32 @main() {", "declare void @printInt(i32)"], Map.empty, 0)

finishState :: LState -> LState
finishState (lines, map, x) = ("}":"ret i32 0":lines, map, x)

-- Code Line Operations

allocateLine :: Ident -> LLabel -> String
allocateLine (Ident name) label = label ++ " = alloca i32 ; variable " ++ name

operationLine :: Exp -> LLabel -> LValue -> LValue -> String
operationLine (ExpAdd _ _) result v1 v2 = result ++ " = add i32 " ++ show v1 ++ ", " ++ show v2
operationLine (ExpSub _ _) result v1 v2 = result ++ " = sub i32 " ++ show v1 ++ ", " ++ show v2
operationLine (ExpMul _ _) result v1 v2 = result ++ " = mul i32 " ++ show v1 ++ ", " ++ show v2
operationLine (ExpDiv _ _) result v1 v2 = result ++ " = sdiv i32 " ++ show v1 ++ ", " ++ show v2
operationLine _ _ _ _ = errorLine

assignLine :: LValue -> LLabel -> String
assignLine value label = "store i32 " ++ show value ++ ", i32* " ++ label

loadLine :: LLabel -> LLabel -> String
loadLine label var = label ++" = load i32, i32* " ++ var

printLine :: LValue -> String
printLine value = "call void @printInt(i32 " ++ show value ++ ")"

emptyLine :: String
emptyLine = ""

errorLine :: String
errorLine = "invalid"

-- Create Mapping Functions

createProgramMapping :: Program -> LCompiler ()
createProgramMapping (Prog stmts) = Prelude.foldl (>>) (return ()) (Prelude.map createStmtMapping stmts)

createStmtMapping :: Stmt -> LCompiler ()
createStmtMapping (SAss ident _) = do
  loc <- lookupIdent ident
  case loc of
    Just _  -> return ()
    Nothing -> do
      label <- getLabelVar
      addLine (allocateLine ident label) >> insertToMap ident label
createStmtMapping _ = return ()

-- Create Functions

createProgram :: Program -> LCompiler ()
createProgram (Prog stmts) = Prelude.foldl (>>) (addLine emptyLine) (Prelude.map createStmt stmts)

createStmt :: Stmt -> LCompiler ()
createStmt (SAss ident exp) = do
  label <- getIdent ident
  value <- createExpression exp
  addLine (assignLine value label)
  addLine emptyLine
createStmt (SExp exp) = do
  value <- createExpression exp
  addLine (printLine value)
  addLine emptyLine

createExpression :: Exp -> LCompiler LValue
createExpression (ExpLit x) = return $ Value x
createExpression (ExpVar ident) = do
  label <- getIdent ident
  labelLine <- getLabelLine
  addLine (loadLine labelLine label) >> return (Label labelLine)

createExpression exp = do
    value1 <- createExpression exp1
    value2 <- createExpression exp2
    label  <- getLabelLine
    addLine (operationLine exp label value1 value2) >> return (Label label)
  where (exp1, exp2) = getSubExpressions exp

-- Generate function

generateProgram :: Program -> IO [String]
generateProgram program = return lines
  where allocState        = execState (createProgramMapping program) initState
        programmedState   = execState (createProgram program) allocState
        (lines, _, _)     = finishState programmedState
