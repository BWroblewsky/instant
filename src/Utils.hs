module Utils where

import Data.List
import System.Environment
import System.Exit hiding (die)
import System.IO
import System.Process

import Instant.AbsInstant
import Instant.ErrM
import Instant.LexInstant
import Instant.ParInstant

getFilename :: IO String
getFilename = getArgs >>= \args -> case args of
  [filename]  -> return filename
  _           -> usage >> die

parseProgram :: String -> IO Program
parseProgram filename = do
  file_content <- readFile filename
  let program_tree = pProgram $ myLexer file_content
  case program_tree of
    Bad err -> do putStrLn "Bad syntax: " >> putStrLn err >> die
    Ok tree -> do return tree

saveProgram :: String -> [String] -> IO()
saveProgram filename lines = writeFile filename (intercalate "\n" (reverse lines))

usage = putStrLn "Usage: ./insc_{llvm|jvm} {program}.ins"
exit  = exitWith ExitSuccess
die   = exitWith (ExitFailure 1)

runCommandWithExit :: String -> IO ()
runCommandWithExit command = runInteractiveCommand command
  >>= \(_pIn, pOut, pErr, handle) -> waitForProcess handle
  >>= \exitCode -> case exitCode of
    ExitSuccess -> return ()
    (ExitFailure _) -> hGetContents pOut >>= putStrLn >> hGetContents pErr >>= putStrLn >> die

runCommandsWithExit :: [String] -> IO()
runCommandsWithExit [] = return ()
runCommandsWithExit (command:commands) = runCommandWithExit command >> runCommandsWithExit commands

getSubExpressions :: Exp -> (Exp, Exp)
getSubExpressions (ExpAdd e1 e2) = (e1, e2)
getSubExpressions (ExpSub e1 e2) = (e1, e2)
getSubExpressions (ExpMul e1 e2) = (e1, e2)
getSubExpressions (ExpDiv e1 e2) = (e1, e2)
getSubExpressions e = (e, e)

isCommutative :: Exp -> Bool
isCommutative (ExpSub _ _) = False
isCommutative (ExpDiv _ _) = False
isCommutative _ = True
