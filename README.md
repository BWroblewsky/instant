## Instant language compilers to LLVM and JVM

### Project structure

##### Makefile
Makefile used to compile the compilers.

##### src
Compilers are written in Haskell. Grammar modules where created and compiled using BNFC tool.

##### lib
Contains 3rd party libraries used by compilator.

- lib/jasmin.jar - jar containing jasmin
- lib/runtime.ll - LLVM library provided in course files on students server.


### Compilation
Compile compilers using make command.

```
make
```

### Using compilers
Compilers are meant to be used on valid Instant source files.

```
./insc_llvm program.ins  # compile to llvm
./insc_jvm program.ins   # compile to jvm
```

##### Credits

Bartłomiej Wróblewski 2018

